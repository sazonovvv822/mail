import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Spy implements MailService {

    private final Logger logger;

    public Spy(Logger logger) {
        this.logger = logger;
    }
    @Override
    public Sendable processMail(Sendable mail) {
        if (mail instanceof MailMessage) {
            MailMessage mailMessage = (MailMessage) mail;
            String from = mailMessage.from;
            String to = mailMessage.to;
            //String[]dd=new String[]{from,to,mailMessage.getMessage()};
            if (from.equals("Austin Powers") || to.equals("Austin Powers")) {
                logger.log(Level.WARNING, "Detected target mail correspondence: from {0} to {1} \"{2}\"",
                        new Object[] { mailMessage.getFrom(), mailMessage.getTo(), mailMessage.getMessage() });
            } else {
                logger.log(Level.INFO, "Usual correspondence: from {0} to {1}",
                        new Object[] { mailMessage.getFrom(), mailMessage.getTo() });
            }
        }
        return mail;
    }
}
