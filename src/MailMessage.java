//Первый класс описывает обычное письмо, в котором находится только текстовое сообщение.
public class MailMessage extends AbstractSendable{ //одна из сущностей Sandable (письмо)
    /*
  Письмо, у которого есть текст, который можно получить с помощью метода `getMessage`
  */
    private final String message;


    public MailMessage(String from, String to, String message) { //конструктор
        super(from, to); //super означает что переменные обязательно
        // должны прийти в этот класс от родительского
        this.message = message; //+ добавляем сам текст СООБЩЕНИЯ
    }

    public String getMessage() { //метод с помощью которого можем вызвать текст СООБЩЕНИЯ
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MailMessage that = (MailMessage) o;

        if (message != null ? !message.equals(that.message) : that.message != null) return false;

        return true;
    }
}
