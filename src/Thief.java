public class Thief implements MailService {
    private int minPrice = 0;
    private int getMinPriceThief = 0;

    public Thief(int minPrice) {
        this.minPrice = minPrice;

    }

    public int getStolenValue(int minPrice) {
        return getMinPriceThief;
    }


    @Override
    public Sendable processMail(Sendable mail) {
        if (mail instanceof MailPackage) {
            MailPackage mailPackage = (MailPackage) mail;
            MailPackage.Package tuk = mailPackage.getContent();
            if (tuk.getPrice() >= minPrice) {
                getMinPriceThief += tuk.getPrice();
                mail = new MailPackage(mail.getFrom(), mail.getTo(), new MailPackage.Package("stones instead of {content}", 0));
            }
        }


        return mail;
    }
}
