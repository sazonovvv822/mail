public class UntrustworthyMailWorker implements MailService {
    private final RealMailService realMailService = new RealMailService();
    private MailService[] mailService;

    public UntrustworthyMailWorker(MailService[] mailService) {
        this.mailService = mailService;
    }

    private MailService getRealMailService() {
        return realMailService;
    }

    @Override
    public Sendable processMail(Sendable mail) {
        Sendable process = mail;
        for (int i = 0; i < mailService.length; i++) {
            process = mailService[i].processMail(process);

        }

        return realMailService.processMail(process);
    }
}