//Второй класс описывает почтовую посылку:
public class MailPackage extends AbstractSendable{ ////одна из сущностей Sandable (посылка)
    /*
Посылка, содержимое которой можно получить с помощью метода `getContent`
*/
    private final Package content;

    public MailPackage(String from, String to, Package content) { //конструктор
        super(from, to);
        this.content = content;
    }


    public  Package getContent() { //СОДЕРЖИМОЕ ПОСЫЛКИ которой можно получить с помощью метода
        return content;
    }

    @Override
    public boolean equals(Object o) { ////переопределенный метод от класса, Object
        // возвращает true тогда и только тогда, когда аргумент не равен нулю
        // и является логическим объектом,
        // представляющим то же логическое значение, что и этот объект.
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MailPackage that = (MailPackage) o;

        if (!content.equals(that.content)) return false;

        return true;
    }
    //При этом сама посылка описывается следующим классом:

    public static class Package {
        /*
        Класс, который задает посылку. У посылки есть текстовое описание содержимого и целочисленная ценность.
        */
        private final String content;
        private final int price;

        public Package(String content, int price) {
            this.content = content;
            this.price = price;
        }

        public String getContent() {
            return content;
        }

        public int getPrice() {
            return price;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Package aPackage = (Package) o;

            if (price != aPackage.price) return false;
            if (!content.equals(aPackage.content)) return false;

            return true;
        }
    }

}
