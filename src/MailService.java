public interface MailService {
    /*
Интерфейс, который задает класс, который может каким-либо образом обработать почтовый объект.
*/
    Sendable processMail(Sendable mail); //что может быть с отправляемой почтой
}
