public class Inspector implements MailService {

    @Override
    public Sendable processMail(Sendable mail) {
        if (mail instanceof MailPackage) {
            MailPackage mailPackage = (MailPackage) mail;
            MailPackage.Package puk = mailPackage.getContent(); // Package puk
            String con = puk.getContent();
            if (con.contains("stones")) {
                throw new StolenPackageException();
            } else if (con.contains("weapons") || con.contains("banned substance")) {
                throw new IllegalPackageException();
            }


        }
        return mail;
    }

    public static class StolenPackageException extends RuntimeException {

    }

    public static class IllegalPackageException extends RuntimeException {

    }
}
