/*
Абстрактный класс, который позволяет абстрагировать логику хранения
источника и получателя письма в соответствующих полях класса.
*/
public  abstract class AbstractSendable implements Sendable { //хранения в String строчках от кого и кому
    //и как получить если нужно(вызвать)
    protected final String from;  // от кого
    protected final String to;  // кому

    public AbstractSendable(String from, String to) {    //конструктор
        this.from = from;
        this.to = to;
    }

    @Override
    public String getFrom() {  //переопределенный метод от интерфейса (от кого)  , получить информацию
        return from;
    }

    @Override
    public String getTo() { //переопределенный метод от интерфейса (кому письмо)  , получить информацию
        return to;
    }

    @Override
    public boolean equals(Object o) {  //переопределенный метод от класса, Object
        // возвращает true тогда и только тогда, когда аргумент не равен нулю и является логическим объектом,
        // представляющим то же логическое значение, что и этот объект.

        if (this == o) return true;//Посмотрим на метод equals(). Первое сравнение сравнивает текущий экземпляр
        // Объекта this с переданным объектом o. Если это один и тот же объект, то equals() вернёт true

        if (o == null || getClass() != o.getClass()) return false;
       // Во втором сравнении проверяется, является ли переданный объект null и какой у него тип.
        // Если переданный объект другого типа, то объекты не равны.



                AbstractSendable that = (AbstractSendable) o;
       // Наконец, equals() сравнивает поля объектов. Если два объекта имеют одинаковые значения полей,
        // то объекты совпадают.
        if (!from.equals(that.from)) return false;
        if (!to.equals(that.to)) return false;

        return true;
    }
}
